package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import model.Produit;
import service.ServiceProduit;

@Component
public class ProduitProducer {
	private Log logger = LogFactory.getLog(ProduitProducer.class);
	
	private ServiceProduit produitervice;
	
	@Autowired
	public ProduitProducer(ServiceProduit serviceProduit) {
		this.produitervice = serviceProduit;
	}
	
	@PostConstruct
	public void produitData(){
		findProduits();
		addOneProduit();
		findProduits();
	}
	
	private void addOneProduit() {
		logger.info("--> Adding new product now!");
		produitervice.addProduit(new Produit("Iphone", "Nouvelle Iphone X", 1200.50));
	}
	
	private void findProduits() {
		logger.info("Trying to find all products!");
		
		List<Produit> allProduits = produitervice.getAllProduits();
		
		if(allProduits.isEmpty()) {
			logger.info("--No products found");
		}else {
			for(Produit p : allProduits) {
				logger.info(String.format("product with id %d and name %s found :, args)",p.getId(),p.getLibelle()));
			}
		}
	}

}
