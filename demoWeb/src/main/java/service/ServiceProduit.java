package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import model.Produit;
import repository.ProduitRepository;

@Service
@Transactional
public class ServiceProduit {
	
	@Autowired
	private ProduitRepository produitRepository;
	
	@Transactional(readOnly = true)
	public List<Produit> getAllProduits(){
		return produitRepository.findAll();
	}
	
	public void addProduit(Produit produit) {
		produitRepository.save(produit);
	}
	
	public void deleteProduit(Long id) {
		produitRepository.deleteById(id);
	}
}
