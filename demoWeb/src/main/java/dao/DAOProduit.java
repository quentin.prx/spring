package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Produit;
import util.Context;

public class DAOProduit {
	public void insert(Produit f) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		em.persist(f);

		em.getTransaction().commit();
		em.close();
		Context.destroy();

	}

	public Produit update(Produit f) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		em.merge(f);

		em.getTransaction().commit();
		em.close();
		Context.destroy();

		return f;
	}

	public Produit selectById(int id)
	{
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Produit a = em.find(Produit.class, id);

		em.close();
		Context.destroy();
		return a;
	}
	
	
	public List<Produit> selectAll()
	{
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("SELECT f FROM Produit f");
		List<Produit> Produits = query.getResultList();
		

		em.close();
		Context.destroy();
		return Produits;
	}

	public void delete(Produit f)
	{
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		f=em.merge(f);
		em.remove(f);

		em.getTransaction().commit();

		em.close();
		Context.destroy();
	}
}
