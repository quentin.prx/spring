package repository;

///import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demoWeb.DemoWebApplication;

import static org.fest.assertions.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = DemoWebApplication.class)
public class ProduitRepositoryTest {

	@Autowired
	private ProduitRepository produitRepository;
	
	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(produitRepository.findAll()).hasSize(1);
	}
	
	@Test
	public void findByLibelleAndPrix_andNoneFound() {
		assertThat(produitRepository.findByLibelleAndPrix("android", 10)).isNull();
	}
	
	@Test 
	public void findByibelleAndPrix_andFoundOne() {
		assertThat(produitRepository.findByLibelleAndPrix("Iphone", 10)).isNotNull();
	}
}
